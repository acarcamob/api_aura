package com.tchile.bigdata.ApiProduct

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.spark.streaming._
import org.apache.spark.streaming.StreamingContext._
import org.apache.log4j.Level
import org.apache.spark.sql.hive.HiveContext
import org.apache.hadoop.fs.Path

import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat

import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.Row

/**
 * Spark ingesta archivo a json
 * El objetivo de este desarrollo es tomar data csv y transformarla a json 
 * para posteriormente enviar a un sitio ftp para que se consulte vía redis
 * 
 * Autor: Rodolfo Maripan /Axel Carcamo
 */

object App{

  def main(args: Array[String]) {
  
  // Rutas de ejecución
  
   val protocol = "hdfs://nn"
   val baseDir = "/stage/api/product/"
   val checkpointFolder = protocol + baseDir + "checkpoint"
   val processingFolder = protocol + baseDir + "landing"
   val database = "api"
   val rawDataTable = "data_product"
   val processedDataTable = "data_product_json"

   val sparkConf = new SparkConf().setAppName("ProductApi").setMaster("local[4]").set("spark.driver.allowMultipleContexts", "true")
    
   // Create the StreamContext class
   val ssc = new StreamingContext(sparkConf, Seconds(30))
   val sc = ssc.sparkContext
   
  /* hive conf*/
  val hiveContext = new org.apache.spark.sql.hive.HiveContext(sc)
  hiveContext.setConf("hive.exec.dynamic.partition", "true")
  hiveContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
  //crea funcion temporal collect_struct_acb
  hiveContext.sql("""create temporary function collect_struct_acb as 'brickhouse.udf.collect.CollectUDAF'""")
     
  def fileNameFilter(path: Path) : Boolean = {
  if (path.getName().contains("COPYING") || path.getName().equalsIgnoreCase("TMP")) { 
      return false 
      } else {
        return true
      }
  }
   
  val dStream = ssc.fileStream [LongWritable, Text, TextInputFormat](processingFolder, x=>fileNameFilter(x), true).map(_._2.toString)
  
    dStream.foreachRDD{
    rdd =>
        { 
          if(rdd.toDebugString.contains("hdfs"))
          {
             val fileName = (rdd.toDebugString.substring(rdd.toDebugString.indexOf("hdfs"), rdd.toDebugString.indexOf("NewHadoopRDD")-1)).trim()
            
             val _sqlContext = SQLContext.getOrCreate(rdd.sparkContext)
           
            val schema = new StructType().add(StructField("id", StringType, true)).
              add(StructField("publicId", StringType, true)).
              add(StructField("name", StringType, true)).
              add(StructField("productType", StringType, true)).
              add(StructField("isBundle", StringType, true)).
              add(StructField("isCustomerVisible", StringType, true)).
              add(StructField("startDate", StringType, true)).
              add(StructField("status", StringType, true)).
              add(StructField("subStatus", StringType, true)).
              add(StructField("productOfferingId", StringType, true)).
              add(StructField("productOfferingName", StringType, true)).
              add(StructField("productOfferingDescription", StringType, true)).
              add(StructField("idProductSpec", StringType, true)).
              add(StructField("nameProductSpec", StringType, true)).
              add(StructField("characteristicName", StringType, true)).
              add(StructField("characteristicValue", StringType, true)).
              add(StructField("recurringChargePeriod", StringType, true)).
              add(StructField("amountPrice", StringType, true)).
              add(StructField("unitsPrice", StringType, true)).
              add(StructField("relatedPartyId", StringType, true)).
              add(StructField("relatedPartyName", StringType, true)).
              add(StructField("relatedPartyRole", StringType, true))

            val regex1 = ".+/".r
            val regex2 = ".+_".r
            val regex3 = ".txt".r
            //obtiene solo el nombre del archivo (borra full path)
            var filenameStr = regex1.replaceAllIn(fileName,"")
            var numeric_date = regex2.replaceAllIn(fileName,"")
            //obtiene la fecha que tiene el archivo (formato numero YYYYMMDD)
            numeric_date = regex3.replaceAllIn(numeric_date,"")
            //año
            var year = numeric_date.substring(0,4)
            //mes
            var month = numeric_date.substring(4,6)
            //dia
            var day = numeric_date.substring(6,8)
            //fecha en formato YYYY-MM-DD
            var file_date = year.concat("-").concat(month).concat("-").concat(day)

            val rowsRdd2 = rdd.map(x => x.split('|')).map(x => Row.fromSeq(x))
            val dfdata = _sqlContext.createDataFrame(rowsRdd2,schema)
            //agrega columna con fecha particion
            val newdf2 = dfdata.withColumn("pd",lit(file_date))
            // borra particion en caso que ya exista 
            var drop_pd="alter table " + database + "." + rawDataTable + " drop if exists partition(pd='" + file_date + "')"
            hiveContext.sql(drop_pd)
            //agrega data a tabla
            newdf2.write.mode("append").partitionBy("pd").saveAsTable(database + "." + rawDataTable)

            //escribe archivo salida
            val csv_dir = "/stage/api/product/archive/" + year 
            var csv_output_path = "hdfs://"+ csv_dir + "/csv_out/" + file_date
            dfdata.coalesce(1).write.format("com.databricks.spark.csv").option("codec","org.apache.hadoop.io.compress.GzipCodec").option("header", "false").option("delimiter","|").save(csv_output_path)
            
            // borra tabla temporal data_product_json
            //hiveContext.sql("drop table if exists " + database + "." + processedDataTable)
            // inserta en tabla temporal data_product_json data agrupada
            //hiveContext.sql("create table api.data_product_json as select id, publicId, name, productType, isBundle, isCustomerVisible, startDate, status, subStatus, productOffering, productSpec, characteristic, productPrice, relatedParty from (select id, publicId, name, productType, isBundle, isCustomerVisible, startDate, status, subStatus, named_struct('id',productOfferingId,'name',productOfferingName,'description',productOfferingDescription) as productOffering, named_struct('id',idProductSpec,'name',nameProductSpec) as productSpec, collect_struct_acb(named_struct('name', characteristicName,'value',characteristicValue)) over(partition by id) as characteristic, named_struct('recurringChargePeriod',recurringChargePeriod,'price',named_struct('amount',amountPrice,'units',unitsPrice)) as productPrice, named_struct('id',relatedPartyId,'name',relatedPartyName,'role',relatedPartyRole) as relatedParty from api.data_product where pd='" + file_date + "' ) AA group by id, publicId, name, productType, isBundle, isCustomerVisible, startDate, status, subStatus, productOffering, productSpec, characteristic, productPrice, relatedParty")
            //crea json
            //val df_tojson = hiveContext.sql("select id, publicId, name, productType, isBundle, isCustomerVisible, startDate, status, subStatus, productOffering, productSpec, characteristic, productPrice, relatedParty from api.data_product_json")
            
            //var json_dir = "/stage/api/product/archive/" + year 
            //var json_output_path = "hdfs://"+ json_dir + "/json_out/" + file_date
            
            //json a archivo
            //df_tojson.toJSON.coalesce(1).saveAsTextFile(json_output_path)
            
            // borra tabla temporal data_product_json
            //hiveContext.sql("drop table if exists " + database + "." + processedDataTable)
            //hadoop fileSystem
            val fs = org.apache.hadoop.fs.FileSystem.get(rdd.sparkContext.hadoopConfiguration)
            
            val csv_file = fs.globStatus(new Path(csv_dir + "/csv_out/" + file_date + "/part*"))(0).getPath().getName();
            fs.rename(new Path(csv_dir + "/csv_out/" + file_date + "/" + csv_file), new Path(csv_dir + "/csv_out/" + filenameStr + ".gz"));
            
            //val json_file = fs.globStatus(new Path(csv_dir + "/json_out/" + file_date + "/part*"))(0).getPath().getName();
            //fs.rename(new Path(csv_dir + "/json_out/" + file_date + "/" + json_file), new Path(csv_dir + "/json_out/" + filenameStr));
            //borra directorio temporal
            if(fs.exists(new org.apache.hadoop.fs.Path(csv_output_path))){
              fs.delete(new org.apache.hadoop.fs.Path(csv_output_path), true)
            }
            
//            if(fs.exists(new org.apache.hadoop.fs.Path(json_output_path))){
//              fs.delete(new org.apache.hadoop.fs.Path(json_output_path), true)
//            }
            //para borrar archivo subido a hive - se borra archivo original
            //arhivo comprimido se deja en hdfs
            val csv_file_path = "hdfs:///stage/api/product/landing/" + filenameStr
            
            if(fs.exists(new org.apache.hadoop.fs.Path(csv_file_path))){
              fs.delete(new org.apache.hadoop.fs.Path(csv_file_path), true)
            }
                        
          }
        }
    }
    
    println( "Fin conteo lineas" ) 

    ssc.checkpoint(checkpointFolder)
    ssc.start()
    ssc.awaitTermination()
  }
  
}