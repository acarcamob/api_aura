package com.tchile.bigdata.ApiCustomer

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.spark.streaming._
import org.apache.spark.streaming.StreamingContext._
import org.apache.log4j.Level
import org.apache.spark.sql.hive.HiveContext
import org.apache.hadoop.fs.Path

import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat

import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.Row

/**
 * Spark ingesta archivo a json
 * El objetivo de este desarrollo es tomar data csv y transformarla a json 
 * para posteriormente enviar a un sitio ftp para que se consulte vía redis
 * 
 * Autor: Rodolfo Maripan /Axel Carcamo
 */

object App{

  def main(args: Array[String]) {
  
  // Rutas de ejecución
  
   val protocol = "hdfs://nn"
   val baseDir = "/stage/api/customer/"
   val checkpointFolder = protocol + baseDir + "checkpoint"
   val processingFolder = protocol + baseDir + "landing"
   val database = "api"
   val rawDataTable = "data_customer"
   val processedDataTable = "data_customer_json"

   val sparkConf = new SparkConf().setAppName("CustomerApi").setMaster("local[4]").set("spark.driver.allowMultipleContexts", "true")
    
   // Create the StreamContext class
   val ssc = new StreamingContext(sparkConf, Seconds(30))
   val sc = ssc.sparkContext
   
  /* hive conf*/
  val hiveContext = new org.apache.spark.sql.hive.HiveContext(sc)
  hiveContext.setConf("hive.exec.dynamic.partition", "true")
  hiveContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
  //crea funcion temporal collect_struct_acb
  hiveContext.sql("""create temporary function collect_struct_acb as 'brickhouse.udf.collect.CollectUDAF'""")
     
  def fileNameFilter(path: Path) : Boolean = {
  if (path.getName().contains("COPYING") || path.getName().equalsIgnoreCase("TMP")) { 
      return false 
      } else {
        return true
      }
  }
    
  //val dStream = ssc.fileStream [LongWritable, Text, TextInputFormat](processingFolder, x=>fileNameFilter(x), true).map{case (x, y) => (x.toString, y.toString)}
  val dStream = ssc.fileStream [LongWritable, Text, TextInputFormat](processingFolder, x=>fileNameFilter(x), true).map(_._2.toString)
  
    dStream.foreachRDD{
    rdd =>
        { 
          if(rdd.toDebugString.contains("hdfs"))
          {
            println( "1 ..." ) 
             val fileName = (rdd.toDebugString.substring(rdd.toDebugString.indexOf("hdfs"), rdd.toDebugString.indexOf("NewHadoopRDD")-1)).trim()
            println( "2 ..." ) 
             val _sqlContext = SQLContext.getOrCreate(rdd.sparkContext)
           println( "3 ..." ) 
            val schema = new StructType().add(StructField("id", StringType, true)).
              add(StructField("href", StringType, true)).
              add(StructField("name", StringType, true)).
              add(StructField("customerRank", StringType, true)).
              add(StructField("segment", StringType, true)).
              add(StructField("subsegment", StringType, true)).
              add(StructField("type", StringType, true)).
              add(StructField("satisfaction", StringType, true)).
              add(StructField("contactMediumtype1", StringType, true)).
              add(StructField("contactMediumpreferred1", StringType, true)).
              add(StructField("contactMediumkey1", StringType, true)).
              add(StructField("contactMediumvalue1", StringType, true)).
              add(StructField("contactMediumtype2", StringType, true)).
              add(StructField("contactMediumpreferred2", StringType, true)).
              add(StructField("contactMediumkey2", StringType, true)).
              add(StructField("contactMediumvalue2", StringType, true)).
              add(StructField("country", StringType, true)).
              add(StructField("region", StringType, true)).
              add(StructField("locality", StringType, true)).
              add(StructField("addressName", StringType, true)).
              add(StructField("addressNumber", StringType, true)).
              add(StructField("postalCode", StringType, true)).
              add(StructField("creditProfileDate", StringType, true)).
              add(StructField("creditScore", StringType, true)).
              add(StructField("legalIdCountry", StringType, true)).
              add(StructField("isPrimaryIDType", StringType, true)).
              add(StructField("nationalIDType", StringType, true)).
              add(StructField("nationalID", StringType, true)).
              add(StructField("idAccount", StringType, true)).
              add(StructField("nameAccount", StringType, true)).
              add(StructField("statusAccount", StringType, true)).
              add(StructField("isPersonalDataProtectionIndicator", StringType, true))
            println( "4 ..." ) 
            val regex1 = ".+/".r
            val regex2 = ".+_".r
            val regex3 = ".txt".r
            //obtiene solo el nombre del archivo (borra full path)
            var filenameStr = regex1.replaceAllIn(fileName,"")
            var justFileName = regex3.replaceAllIn(filenameStr,"")
            var numeric_date = regex2.replaceAllIn(fileName,"")
            //obtiene la fecha que tiene el archivo (formato numero YYYYMMDD)
            numeric_date = regex3.replaceAllIn(numeric_date,"")
            //año
            var year = numeric_date.substring(0,4)
            //mes
            var month = numeric_date.substring(4,6)
            //dia
            var day = numeric_date.substring(6,8)
            //fecha en formato YYYY-MM-DD
            var file_date = year.concat("-").concat(month).concat("-").concat(day)
            println( "5 ..." ) 
            val rowsRdd2 = rdd.map(x => x.split("""\|""",-1)).map(x => Row.fromSeq(x))
            println( "6 ..." ) 
            val dfdata = _sqlContext.createDataFrame(rowsRdd2,schema)
            println( "6.1 ..." )
            //agrega columna con fecha particion
            val newdf2 = dfdata.withColumn("pd",lit(file_date))
            println( "6.2 ..." )
            // borra particion en caso que ya exista 
            var drop_pd="alter table " + database + ".data_customer drop if exists partition(pd='" + file_date + "')"
            println( "6.3 ..." )
            hiveContext.sql(drop_pd)
            println( "6.4 ..." )
            //agrega data a tabla
            println(newdf2.show())
            newdf2.write.mode("append").partitionBy("pd").saveAsTable(database + "." + rawDataTable)
            println( "7 ..." ) 
            //escribe archivo salida
            var csv_dir = "/stage/api/customer/archive/" +  year
            var csv_output_path = "hdfs://"+ csv_dir + "/csv_out/" + file_date
            dfdata.coalesce(1).write.format("com.databricks.spark.csv").option("codec","org.apache.hadoop.io.compress.GzipCodec").option("header", "false").option("delimiter","|").save(csv_output_path)
            println( "8 ..." ) 
            // borra tabla temporal data_customer_json
            hiveContext.sql("drop table if exists " + database + "." + processedDataTable)
            // inserta en tabla temporal data_customer_json data agrupada
            hiveContext.sql("create table api.data_customer_json as select id, case when isnull(href) then '' else href end as href, name, customerrank, segment, subsegment, type, satisfaction, contactMedium, customerAddress, customerCreditProfile, legalId, customerAccount, additionalData from (select id, href, name, customerrank, segment, subsegment, type, satisfaction, array(named_struct('type',contactmediumtype1,'preferred',contactmediumpreferred1,'medium',named_struct('key',contactmediumkey1,'value',case when isnull(contactmediumvalue1) then '' else contactmediumvalue1 end)),named_struct('type',contactmediumtype2,'preferred',contactmediumpreferred2,'medium',named_struct('key',contactmediumkey2,'value',case when isnull(contactmediumvalue2) then '' else contactmediumvalue2 end  ))) as contactMedium, named_struct('country',country,'region',region,'locality',locality,'addressName',addressname,'addressNumber',addressnumber,'postalCode',postalcode) as customerAddress, named_struct('creditProfileDate',creditprofiledate,'creditScore',creditscore) as customerCreditProfile, named_struct('country',legalidcountry,'isPrimary',isprimaryidtype,'nationalIDType',nationalidtype,'nationalID',nationalid) as legalId, collect_struct_acb(named_struct('id', idaccount,'name',nameaccount,'status',statusaccount)) over(partition by id) as customerAccount, named_struct('key','isPersonalDataProtectionIndicator','value',isPersonalDataProtectionIndicator) as additionalData from api.data_customer where pd='" + file_date + "') AA group by id, href, name, customerrank, segment, subsegment, type, satisfaction, contactMedium, customerAddress, customerCreditProfile, legalId, customerAccount, additionalData")
            //crea json
            val df_tojson = hiveContext.sql("select id, href, name, customerrank as customerRank, segment, subsegment,type,satisfaction,contactmedium as contactMedium,customeraddress as customerAddress,customercreditprofile as customerCreditProfile,legalid as legalId, customeraccount as customerAccount, additionaldata as additionalData from api.data_customer_json")
            println( "9 ..." ) 
            var json_dir = "/stage/api/customer/archive/" + year 
            var json_output_path = "hdfs://"+ json_dir + "/json_out/" + file_date
            println( "10 ..." ) 
            //json a archivo
            df_tojson.toJSON.coalesce(1).saveAsTextFile(json_output_path)
            
            // borra tabla temporal data_customer_json
            hiveContext.sql("drop table if exists " + database + "." + processedDataTable)
            
            val servidorHDFS = "hdfs://nn"
            val pathHDFS = servidorHDFS.concat(csv_output_path)
            println( "11 ..." ) 
            val fs = org.apache.hadoop.fs.FileSystem.get(rdd.sparkContext.hadoopConfiguration)
            val csv_file = fs.globStatus(new Path(csv_dir + "/csv_out/" + file_date + "/part*"))(0).getPath().getName();
            fs.rename(new Path(csv_dir + "/csv_out/" + file_date + "/" + csv_file), new Path(csv_dir + "/csv_out/" + filenameStr + ".gz"));
            val json_file = fs.globStatus(new Path(csv_dir + "/json_out/" + file_date + "/part*"))(0).getPath().getName();
            fs.rename(new Path(csv_dir + "/json_out/" + file_date + "/" + json_file), new Path(csv_dir + "/json_out/" + filenameStr));
            
            if(fs.exists(new org.apache.hadoop.fs.Path(csv_output_path))){
              fs.delete(new org.apache.hadoop.fs.Path(csv_output_path), true)
            }
            
            if(fs.exists(new org.apache.hadoop.fs.Path(json_output_path))){
              fs.delete(new org.apache.hadoop.fs.Path(json_output_path), true)
            }
            
            val csv_file_path = "hdfs:///stage/api/customer/landing/" + filenameStr
            
            if(fs.exists(new org.apache.hadoop.fs.Path(csv_file_path))){
              fs.delete(new org.apache.hadoop.fs.Path(csv_file_path), true)
            }
            
          }
        }
    }
    println( "Fin ..." ) 
    ssc.checkpoint(checkpointFolder)
    ssc.start()
    ssc.awaitTermination()
  }
  
}