package com.tchile.bigdata.ApiAllowance

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.spark.streaming._
import org.apache.spark.streaming.StreamingContext._
import org.apache.log4j.Level
import org.apache.spark.sql.hive.HiveContext
import org.apache.hadoop.fs.Path

import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat

import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.Row

/**
 * Spark ingesta archivo a json
 * El objetivo de este desarrollo es tomar data csv y transformarla a json 
 * para posteriormente enviar a un sitio ftp para que se consulte vía redis
 * 
 * Autor: Rodolfo Maripan /Axel Carcamo
 */

object App{

  def main(args: Array[String]) {
  
  // Rutas de ejecución
  
   val protocol = "hdfs://nn"
   val baseDir = "/stage/api/allowance/"
   val checkpointFolder = protocol + baseDir + "checkpoint"
   val processingFolder = protocol + baseDir + "landing"
   val database = "api"
   val rawDataTable = "data_allowance"
   val processedDataTable = "data_allowance_json"

   val sparkConf = new SparkConf().setAppName("AllowanceApi").setMaster("local[4]").set("spark.driver.allowMultipleContexts", "true")
    
   // Create the StreamContext class
   val ssc = new StreamingContext(sparkConf, Seconds(30))
   val sc = ssc.sparkContext
   
  /* hive conf*/
  val hiveContext = new org.apache.spark.sql.hive.HiveContext(sc)
  hiveContext.setConf("hive.exec.dynamic.partition", "true")
  hiveContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
  
  //crea funcion temporal collect_struct_acb
  hiveContext.sql("""create temporary function collect_struct_acb as 'brickhouse.udf.collect.CollectUDAF'""")
     
  def fileNameFilter(path: Path) : Boolean = {
  if (path.getName().contains("COPYING") || path.getName().equalsIgnoreCase("TMP")) { 
      return false 
      } else {
        return true
      }
  }
    
  //val dStream = ssc.fileStream [LongWritable, Text, TextInputFormat](processingFolder, x=>fileNameFilter(x), true).map{case (x, y) => (x.toString, y.toString)}
  val dStream = ssc.fileStream [LongWritable, Text, TextInputFormat](processingFolder, x=>fileNameFilter(x), true).map(_._2.toString)
  
    dStream.foreachRDD{
    rdd =>
        { 
          if(rdd.toDebugString.contains("hdfs"))
          {
             val fileName = (rdd.toDebugString.substring(rdd.toDebugString.indexOf("hdfs"), rdd.toDebugString.indexOf("NewHadoopRDD")-1)).trim()
            
             val _sqlContext = SQLContext.getOrCreate(rdd.sparkContext)
           
            val schema = new StructType().add(StructField("customer_key", StringType, true)).
                add(StructField("subscriber_key", StringType, true)).
                add(StructField("product_desc", StringType, true)).
                add(StructField("effective_date", StringType, true)).
                add(StructField("expiration_date", StringType, true)).
                add(StructField("allowance_type", StringType, true)).
                add(StructField("voice_quota_seconds", StringType, true)).
                add(StructField("data_quota_kbytes", StringType, true)).
                add(StructField("sms_quota_q", StringType, true)).
                add(StructField("monetary_quota", StringType, true)).
                add(StructField("primary_resource_value", StringType, true)).
                add(StructField("price", StringType, true)).
                add(StructField("duration_days", StringType, true)).
                add(StructField("product_key", StringType, true)).
                add(StructField("billing_offer_instance_id", StringType, true)).
                add(StructField("status", StringType, true)).
                add(StructField("product_spec_id", StringType, true)).
                add(StructField("product_spec_name", StringType, true))

            val regex1 = ".+/".r
            val regex2 = ".+_".r
            val regex3 = ".txt".r
            //obtiene solo el nombre del archivo (borra full path)
            var filenameStr = regex1.replaceAllIn(fileName,"")
            var numeric_date = regex2.replaceAllIn(fileName,"")
            //obtiene la fecha que tiene el archivo (formato numero YYYYMMDD)
            numeric_date = regex3.replaceAllIn(numeric_date,"")
            //año
            var year = numeric_date.substring(0,4)
            //mes
            var month = numeric_date.substring(4,6)
            //dia
            var day = numeric_date.substring(6,8)
            //fecha en formato YYYY-MM-DD
            var file_date = year.concat("-").concat(month).concat("-").concat(day)

            val rowsRdd2 = rdd.map(x => x.split('|')).map(x => Row.fromSeq(x))
            val dfdata = _sqlContext.createDataFrame(rowsRdd2,schema)
            //agrega columna con fecha particion
            val newdf2 = dfdata.withColumn("pd",lit(file_date))
            // borra particion en caso que ya exista 
            var drop_pd="alter table " + database + "." + rawDataTable + " drop if exists partition(pd='" + file_date + "')"
            hiveContext.sql(drop_pd)
            //agrega data a tabla
            newdf2.write.mode("append").partitionBy("pd").saveAsTable(database + "." + rawDataTable)

            //escribe archivo salida
            val csv_dir = "/stage/api/allowance/archive/" + year 
            var csv_output_path = "hdfs://"+ csv_dir + "/csv_out/" + file_date
            dfdata.coalesce(1).write.format("com.databricks.spark.csv").option("codec","org.apache.hadoop.io.compress.GzipCodec").option("header", "false").option("delimiter","|").save(csv_output_path)
            
            // borra tabla temporal data_allowance_json
//            hiveContext.sql("drop table if exists " + database + "." + processedDataTable)
            // inserta en tabla temporal data_allowance_json data agrupada
//            hiveContext.sql("create table api.data_allowance_json as select billing_offer_instance_id as id, primary_resource_value as publicId, 'Mobile' as name, allowance_type as productType, 'false' as isBundle, 'true' as isCustomerVisible, effective_date as startDate, 'Active' as status, '' as subStatus, named_struct('id', product_key,'name','','description','Prepaid') as productOffering, named_struct('id',product_spec_id,'name',product_spec_name) as productSpec, ARRAY(named_struct('name', 'Cantidad acumulada de segundos','value',voice_quota_seconds),named_struct('name', 'Cantidad acumulada de kb','value',data_quota_kbytes),named_struct('name', 'Cantidadacumulada de sms','value',sms_quota_q),named_struct('name', 'Cantidad acumulada de monetaria','value',monetary_quota)) as characteristic, named_struct('recurringChargePeriod','','price',named_struct('amount',price,'units','CLP')) as productPrice, named_struct('id',customer_key,'name','Mig Anonymous Caller','role','customer') as relatedParty FROM api.data_allowance where pd='" + file_date + "'")
            //crea json
//            val df_tojson = hiveContext.sql("select id, publicId, name, productType, isBundle, isCustomerVisible, startDate, status, subStatus, productOffering, productSpec, characteristic, productPrice, relatedParty from api.data_allowance_json")
            
//            var json_dir = "/stage/api/allowance/archive/" + year 
//            var json_output_path = "hdfs://"+ json_dir + "/json_out/" + file_date
            
            //json a archivo
//            df_tojson.toJSON.coalesce(1).saveAsTextFile(json_output_path)
            
            // borra tabla temporal data_allowance_json
//            hiveContext.sql("drop table if exists " + database + "." + processedDataTable)
            
            val fs = org.apache.hadoop.fs.FileSystem.get(rdd.sparkContext.hadoopConfiguration)
            
            val csv_file = fs.globStatus(new Path(csv_dir + "/csv_out/" + file_date + "/part*"))(0).getPath().getName();
            
             if(fs.exists(new Path(csv_dir + "/csv_out/" + filenameStr + ".gz"))){
              fs.delete(new Path(csv_dir + "/csv_out/" + filenameStr + ".gz"), false)
            }
             
            fs.rename(new Path(csv_dir + "/csv_out/" + file_date + "/" + csv_file), new Path(csv_dir + "/csv_out/" + filenameStr + ".gz"));
//            val json_file = fs.globStatus(new Path(csv_dir + "/json_out/" + file_date + "/part*"))(0).getPath().getName();
//            fs.rename(new Path(csv_dir + "/json_out/" + file_date + "/" + json_file), new Path(csv_dir + "/json_out/" + filenameStr));
            
            if(fs.exists(new org.apache.hadoop.fs.Path(csv_output_path))){
              fs.delete(new org.apache.hadoop.fs.Path(csv_output_path), true)
            }
            
//            if(fs.exists(new org.apache.hadoop.fs.Path(json_output_path))){
//              fs.delete(new org.apache.hadoop.fs.Path(json_output_path), true)
//            }
            //borra archivo original, solo se deja archivo comprimido
            val csv_file_path = "hdfs:///stage/api/allowance/landing/" + filenameStr
            
            if(fs.exists(new org.apache.hadoop.fs.Path(csv_file_path))){
              fs.delete(new org.apache.hadoop.fs.Path(csv_file_path), true)
            }
            
            // codigo de product_allowance
            var max_pd_product =hiveContext.sql("select max(pd) from api.data_product")
            var max_pd_allowance =hiveContext.sql("select max(pd) from api.data_allowance")
            var str_pd_product = max_pd_product.head().getString(0)
            var str_pd_allowance = max_pd_allowance.head().getString(0)
            hiveContext.sql("drop table if exists api.data_product_allowance_tmp")
            hiveContext.sql("create table api.data_product_allowance_tmp as select id, publicId, name, productType, isBundle, isCustomerVisible, startDate, status, subStatus, productOffering, productSpec, characteristic, productPrice, relatedParty, customerId from (select id as id, publicId as publicId, name as name, productType as productType, isBundle as isBundle, isCustomerVisible as isCustomerVisible, startDate as startDate, status as status, subStatus as subStatus, named_struct('id',productOfferingId,'name',productOfferingName,'description',productOfferingDescription) as productOffering, named_struct('id',idProductSpec,'name',nameProductSpec) as productSpec, collect_struct_acb(named_struct('name', characteristicName,'value',characteristicValue)) over(partition by id) as characteristic, named_struct('recurringChargePeriod',recurringChargePeriod,'price',named_struct('amount',amountPrice,'units',unitsPrice)) as productPrice, Array(named_struct('id',relatedPartyId,'name',relatedPartyName,'role',relatedPartyRole)) as relatedParty, relatedPartyId as customerId from api.data_product where pd = '" + str_pd_product + "') QQ group by id, publicId, name, productType, isBundle, isCustomerVisible, startDate, status, subStatus, productOffering, productSpec, characteristic, productPrice, relatedParty, customerId")
            val data_allowance = hiveContext.sql("select billing_offer_instance_id as id, primary_resource_value as publicId, 'Mobile' as name, allowance_type as productType, 'false' as isBundle, 'true' as isCustomerVisible, effective_date as startDate, 'Active' as status, '' as subStatus, named_struct('id', product_key,'name','','description','Prepaid') as productOffering, named_struct('id',product_spec_id,'name',product_spec_name) as productSpec, ARRAY(named_struct('name', 'Cantidad acumulada de segundos','value',voice_quota_seconds),named_struct('name', 'Cantidad acumulada de kb','value',data_quota_kbytes),named_struct('name', 'Cantidadacumulada de sms','value',sms_quota_q),named_struct('name', 'Cantidad acumulada de monetaria','value',monetary_quota)) as characteristic, named_struct('recurringChargePeriod','','price',named_struct('amount',price,'units','CLP')) as productPrice, Array(named_struct('id',customer_key,'name','Mig Anonymous Caller','role','customer')) as relatedParty, customer_key as customerId from api.data_allowance where pd = '" + str_pd_allowance + "'")
            data_allowance.write.mode("append").saveAsTable("api.data_product_allowance_tmp")
            
            hiveContext.sql("drop table if exists api.data_product_allowance_json_tmp")
            hiveContext.sql("create table api.data_product_allowance_json_tmp as select customerId, named_struct('code','0','description','Transaccion exitosa') as serviceStatus, collect_struct_acb(named_struct('id', id,'publicId', publicId,'name', name,'productType', productType,'isBundle', isBundle,'isCustomerVisible', isCustomerVisible,'startDate', startDate,'status', status,'subStatus', subStatus,'productOffering',productOffering,'productSpec',productSpec,'characteristic',characteristic,'productPrice',productPrice,'relatedParty',relatedParty)) as product from api.data_product_allowance_tmp group by customerId")
            
            //crea json
            val df_tojson = hiveContext.sql("select serviceStatus,product from api.data_product_allowance_json_tmp")
            
            var json_dir = "/stage/api/product_allowance/archive/" + year 
            var json_output_path = "hdfs://"+ json_dir + "/json_out/" + file_date
            
            //json a archivo
            df_tojson.toJSON.coalesce(1).saveAsTextFile(json_output_path)
            
            val csv_dirOut = "/stage/api/product_allowance/archive/" + year 
            var filenameStrOut = "product_" + numeric_date +  ".txt"
            
            //obtiene path archivo json
            val json_file = fs.globStatus(new Path(csv_dirOut + "/json_out/" + file_date + "/part*"))(0).getPath().getName();
            //renombra archivo part* como product_yyyymmdd.txt en directorio /stage/api/product_allowance/archive/YYYY/json_out
            fs.delete(new Path(csv_dirOut + "/json_out/" + filenameStrOut), true)
            fs.rename(new Path(csv_dirOut + "/json_out/" + file_date + "/" + json_file), new Path(csv_dirOut + "/json_out/" + filenameStrOut));
            fs.copyToLocalFile(new Path(csv_dirOut + "/json_out/" + filenameStrOut),new Path("/tmp/api_product_allowance/"))
              
            //borra directorio temporal donde se genera el json.
            if(fs.exists(new org.apache.hadoop.fs.Path(json_output_path))){
              fs.delete(new org.apache.hadoop.fs.Path(json_output_path), true)
            }
            
            // borra tablas temporales
            hiveContext.sql("drop table if exists api.data_product_allowance_tmp")
            hiveContext.sql("drop table if exists api.data_product_allowance_json_tmp")
            
                        
          }
          
        }
    }
    
    println( "Fin conteo lineas" ) 

    ssc.checkpoint(checkpointFolder)
    ssc.start()
    ssc.awaitTermination()
  }
  
}